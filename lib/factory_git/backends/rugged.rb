require "rugged"

module FactoryGit
  module Backends
    class Rugged < Base
      def commit_file
        # No-op yet.
      end

      def commit_removal
        # No-op yet.
      end

      def create_branch
        # No-op yet.
      end

      def create_repository
        ::Rugged::Repository.init_at(FactoryGit.config.path, :bare)
      end
    end
  end
end
