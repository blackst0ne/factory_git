module FactoryGit
  module Backends
    class Base
      def initialize
        check_path
      end

      private

      #
      # Backend API layer.
      #
      def check_path
        FileUtils.mkdir_p(FactoryGit.config.path)
      end
    end
  end
end
