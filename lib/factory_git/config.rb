module FactoryGit
  class Config
    attr_accessor :backend, :path

    def initialize(*args)
      @backend = :rugged
      @path    = "./tmp/factory_git/"
    end
  end
end
