require "logger"

require_relative "factory_git/backends/base"

require_relative "factory_git/config"
require_relative "factory_git/exceptions"
require_relative "factory_git/version"

module FactoryGit
  class << self
    # FactoryGit supports multiple git-backends. By default `rugged`` is used.
    # But it must be possible to add another backend by just adding one more file
    # to the `backends` directory.
    # All backends must implement this set of methods.
    FACTORY_METHODS = %w(
      commit_file
      commit_removal
      create_branch
      create_repository
    ).freeze

    FACTORY_METHODS.each do |method_name|
      define_method(method_name) do
        backend.public_send(method_name)
      rescue NoMethodError
        raise NotImplementedError, "'#{method_name}' method is not found. It must be implemented in '#{backend.class}'"
      end
    end

    def config
      @config ||= Config.new
    end

    def configure
      yield(config) if block_given?
    end

    def logger
      @logger ||= Logger.new(STDOUT)
    end

    private

    def backend
      require_relative "factory_git/backends/#{config.backend}"

      klass = Module.const_get("FactoryGit::Backends::#{config.backend.to_s.capitalize}")
      @backend ||= klass.new
    rescue LoadError
      raise UnknownBackend, "Unknown backend: #{backend}"
    end
  end
end
