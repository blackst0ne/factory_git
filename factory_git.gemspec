
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "factory_git/version"

Gem::Specification.new do |spec|
  spec.name          = "factory_git"
  spec.version       = FactoryGit::VERSION
  spec.authors       = ["blackst0ne"]
  spec.email         = ["blackst0ne.ru@gmail.com"]

  spec.summary       = %q{A library for setting up VCS objects as test data}
  spec.description   = %q{Factory Version Control System.
                          A library for setting up VCS objects as test data.
                          Just like `factory_bot`}
  spec.homepage      = "https://gitlab.com/blackst0ne/factory_git"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "rugged"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
